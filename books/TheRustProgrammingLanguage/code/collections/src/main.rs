use std::collections::HashMap;

fn main() {
	let mut v: Vec<i32> = Vec::new();

	let mut w = vec![1, 2, 3];

    println!("{:?} {:?}", v, w);

    v.push(5);
    w.push(5);
    v.push(6);
    w.push(6);
    v.push(7);
    w.push(7);
    v.push(8);
    w.push(8);

    println!("{:?} {:?}", v, w);

    let mut v = vec![1, 2, 3, 4, 5];

    let third: &i32 = &v[2];
    println!("The third element is {}", third);

    match v.get(2) {
        Some(third) => println!("The third element is {}", third),
        None => println!("There is no third element."),
    }

    // let does_not_exist = &v[100];
    let does_not_exist = v.get(100);

    let first = v[0];

    v.push(6);

    println!("The first element is: {}", first);

    for i in &v {
        println!("{}", i);
    }

    let mut v = vec![
    	String::from("1"), 
    	String::from("2"), 
    	String::from("3"), 
    	String::from("4"), 
    	String::from("5")];

    //let first = &v[0]; it is funny, sure.
    //let first = v[0];  it is funny, sure.

    v.push(String::from("6"));

    println!("The first element is: {}", first);

    let mut v = vec![
    	"1", 
    	"2", 
    	"3", 
    	"4", 
    	"5"];

    let first = v[0]; //it is funny, sure.
    //let first = v[0];  it is funny, sure.

    v.push("6");

    println!("The first element is: {}", first);

    let mut v = vec![100, 32, 57, 34];
    for i in &mut v {
        *i += 50;
    }

    println!("The vector: {:?}", v);

    #[derive(Debug)]
    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }

    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];

	println!("The row: {:?}", row); 

	let hello = String::from("السلام عليكم");
    let hello = "Dobrý den".to_string();
    let hello = String::from("Hello");
    let hello = String::from("שָׁלוֹם");
    let hello = String::from("नमस्ते");
    println!("{:#?}", hello);
    let hello = "こんにちは".to_string();
    let hello = String::from("안녕하세요");
    let hello = String::from("你好");
    let hello = String::from("Olá");
    let hello = "Здравствуйте".to_string();
    println!("{:#?}", hello);
    let mut hello = String::from("Hola");
    hello.push_str("michel Здравствуйте ");
    println!("{:#?}", hello);

    for c in "नमस्ते".chars() {
	    println!("{}", c);
	}

	let mut i = 0;
	while i < 11 {
		/*let mut input = String::new();
		std::io::stdin().read_line(&mut input).expect("Failed to read line");
		print!("{}\n", input);*/
		i += 1;
	}

	let mut sscores = HashMap::new();

    sscores.insert(String::from("Blue"), 10);
    sscores.insert(String::from("Yellow"), 50);
    sscores.insert(3.to_string(), 50);

    println!("{:?}", sscores);

    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];

    let mut scores: HashMap<_, _> =
        teams.into_iter().zip(
        	initial_scores.into_iter()
        ).collect();

    println!("{:?}", scores);

    let field_name = String::from("Favorite color");
    let field_value = String::from("Blue");

    let mut map = HashMap::new();
    map.insert(field_name, field_value);

	println!("{:?}", map);

	let team_name = String::from("Blue");
    let score = scores.get(&team_name);
    println!("{:?}", score);

    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Blue"), 25);

    println!("{:?}", scores);

    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);

    scores.entry(String::from("Yellow")).or_insert(50);
    scores.entry(String::from("Blue")).or_insert(50);

    println!("{:?}", scores);


    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);

    scores.entry(String::from("Yellow")).or_insert(50);
    *scores.entry(String::from("Blue")).or_insert(50) *= 60;
    println!("{:?}", scores);
}
