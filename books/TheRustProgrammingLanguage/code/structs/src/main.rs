fn main() {
    println!("Hello, Structs !!!!");
    let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    println!("{:?}, {:?}, {:?}, {:?}", 
    	user1.email,
    	user1.username,
    	user1.active,
    	user1.sign_in_count
    );

    let user2 = User {
    	email: String::from("michel@michel.com"),
    	..user1
    };

    println!("{:?}, {:?}, {:?}, {:?}", 
    	user2.email,
    	user2.username,
    	user2.active,
    	user2.sign_in_count
    );

    let blacky = Color(0, 340, 0);
    println!("{:?}, {:?}, {:?}", blacky.0, blacky.1, blacky.2);

    let subject = AlwaysEqual;
    println!("{:?} ", subject);

    let user1 = UserAlt {
        active: true,
        sign_in_count: 1,
    };

    println!("{:?} ", user1);

}

struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64,
}

#[derive(Debug)]
struct UserAlt {
    active: bool,
    sign_in_count: u64,
    //email: &str, compiler need lifetime
    //username: &str, compiler need lifetime
}

struct Color(i32, i32, i32);

#[derive(Debug)]
struct AlwaysEqual;

fn build_user(email: String, username: String) -> User {
	// A build_user function that uses field init shorthand 
	// because the email and username parameters have the 
	// same name as struct fields
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}