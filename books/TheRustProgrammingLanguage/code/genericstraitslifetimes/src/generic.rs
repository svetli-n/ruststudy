/*
	Trying to mix using generic types in vector and points.

	Run using:
		rustc -o targetmichelexe generic.rs && ./targetmichelexe && rm targetmichelexe

		&& rm generic.rs
*/


#[derive(Debug, Copy, Clone)]
enum GenericType {
	tf32(f32),
	tf64(f64),
	ti32(i32),
	ti64(i64),
	tchar(char),

	// tstr(str),
	// tString(String),

	// Compilation fails here , cause : 
	// tstr(str) --- this field does not implement `Copy`
	// tString(String) --- this field does not implement `Copy`

	/*
		My question here is how to solve ?
		I was reading and I find something like: 
			pub trait Detail
		
		in https://stackoverflow.com/questions/38215753/how-do-i-implement-copy-and-clone-for-a-type-that-contains-a-string-or-any-type

		But I am not a trait Rust specialist and I can not
		imagine some creative and easy solution solution.

		Exists ???

		What book to read ??
	*/
}

#[derive(Debug, Copy, Clone)]
struct Point<X, Y> {
    x: X,
    y: Y,
}

impl<X: std::fmt::Debug, Y: std::fmt::Debug> Point<X, Y> {
    fn mix_samex_useyfromarg<XT, YT>(self, other: &Point<XT, YT>) -> Point<X, YT> 
    	// Here does not exits an OR || 
    	// how to have optional traits , is the question ? to solve String
    	where YT: Copy
	    {
	    	Point {
	            x: self.x,
	            y: other.y,
	        }
	    }

    fn printpoint(self){
    	println!("Point {{x: {:?}, y:{:?}}}", self.x, self.y);
    }
}

fn trying_mix(){
	let mut cases : Vec <Point<GenericType, GenericType>> = Vec::from([
		Point { x: GenericType::tf32(1 as f32), y: GenericType::tf32(2 as f32)},
		Point { x: GenericType::ti32(1111 as i32), y: GenericType::tf32(2222 as f32)},
		Point { x: GenericType::tchar('M'), y: GenericType::tf32(2 as f32)},
		Point { x: GenericType::tchar('U'), y: GenericType::tf64(8888 as f64)},
		Point { x: GenericType::ti32(9000 as i32), y: GenericType::tf64(9999 as f64)},

		// Related with the trait copy . 
		// Point { x: GenericType::tString(String::from("Michel")), y: GenericType::tchar('M')},
		// Point { x: GenericType::tstr(""), y: GenericType::tString(String::from("Michel"))},
	]);
	
	let lenCases = cases.len();

	for i in 0..lenCases {
		for j in 0..lenCases {
			cases[i].printpoint();
			cases[j].printpoint();
			println!("The mix was : {:?}", cases[i].mix_samex_useyfromarg(&cases[j]));
		}
	}
}

fn main() {
	trying_mix();
}