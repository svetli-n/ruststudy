fn largest_i32(list: &[i32]) -> i32 {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

fn largest_char(list: &[char]) -> char {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

/*
To fix
fn largest<T: std::cmp::PartialOrd >(list: &[T]) -> T {
    let mut largest = &list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}*/

struct Foo<T: Ord> {
	arr: Vec<T>,
}

//I CAN USE Generics in structs.
#[derive(Debug, Copy, Clone)]
struct Point<X, Y> {
    x: X,
    y: Y,
}

// Interesting, you should use all the generics.
impl<X: std::fmt::Debug, Y: std::fmt::Debug> Point<X, Y> {
    fn mix_samex_useyfromarg<XT, YT>(self, other: &Point<XT, YT>) -> Point<X, YT> 
    	// Here does not exits an OR || 
    	// how to have optional traits , is the question ?
    	where YT: Copy
	    {
	    	Point {
	            x: self.x,
	            y: other.y,
	        }
	    }

    fn printpoint(self){
    	println!("Point {{x: {:?}, y:{:?}}}", self.x, self.y);
    }
}



impl Point<f32, f32> {
	fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}



fn main() {
    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest_i32(&number_list);
    println!("The largest number is {}", result);

    let number_list = vec!['y', 'm', 'a', 'q'];

    let result = largest_char(&number_list);
    println!("The largest number is {}", result);

    // Playing with generics
    let both_integer = Point { x: 2, y: 4 };
    let integer_and_float = Point { x: 4, y: 8.0 };
    let both_float_32 = Point{ x: 5 as f32, y: 10 as f32 };
    let both_float_64 = Point{ x: 6 as f64, y: 12 as f64 };
    let both_str = Point{x: "Michel", y: "MS"};
    let both_string = Point{x: String::from("Michel"), y: String::from("MS")};

    //let mix = both_integer.mix_samex_useyarg(&both_string);
    //mix.printpoint();


	/*
    both_int	eger
	both_float
	integer_and_float
	both_float_32
	both_float_32_2
	both_str
	both_string
*/
    // Interesting concept, sure.
    // println!("{:?}", integer_and_float.distance_from_origin());
    println!("Michelcito");
}


