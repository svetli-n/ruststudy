use std::fs::File;
use std::io::{self, ErrorKind, Read};

fn read_username_from_file_v1() -> Result<String, io::Error> {
	// Understanding the ? operator , but it is not clear 
	// for me.
    let mut f = File::open("hello.txt")?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}

fn read_username_from_file_v2() -> Result<String, io::Error> {
    let mut s = String::new();
    // double sign ?? be carefull cowboy.
    File::open("hello.txt")?.read_to_string(&mut s)?;
    Ok(s)
}

fn testclosure()-> {
	
}

fn main() {
    let f = File::open("hello.txt");

    // The match mechanism is really interesting, sure.
    let f = match f {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating the file: {:?}", e),
            },
            other_error => {
                panic!("Problem opening the file: {:?}", other_error)
            }
        },
    };

    // closures unwrap_or_else
    let f = File::open("hello.txt").unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create("hello.txt").unwrap_or_else(|error| {
                panic!("Problem creating the file: {:?}", error);
            })
        } else {
            panic!("Problem opening the file: {:?}", error);
        }
    });

    let t = read_username_from_file_v2();
    println!("v1 = {:?}", t);
    let t = read_username_from_file_v1();
    println!("v1 = {:?}", t);

    // panic!("crash and burn");
}